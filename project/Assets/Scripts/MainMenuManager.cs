﻿using UnityEngine;
using System.Collections;

public class MainMenuManager{
	
	
	private static MainMenuManager instance;
	
	public static MainMenuManager Instance
	{
		get
		{
			if (instance == null)
				instance = new MainMenuManager();
			return instance;
		}
	}
	
	private MainMenuManager (){
		mainMenu = GameObject.Find("MainMenu");
	}
	
	private GameObject mainMenu;
	
	public void Initialize(){
		mainMenu.renderer.enabled = true;
		SetCollider(mainMenu,true);
	}
	
	public void Close(){
		mainMenu.renderer.enabled = false;
		SetCollider(mainMenu,false);
	}
		
	private void SetCollider(GameObject gameObject, bool value){
		foreach (Transform child in gameObject.transform){
			if(child.tag.Equals("Touchable"))
				child.collider.enabled = value;
			if(child.childCount != 0)
				SetCollider( child.gameObject, value);			
		}
	}
	
}
