﻿using UnityEngine;
using System.Collections;

public class FurnitureEditMoveManager {

	private static FurnitureEditMoveManager instance;
	
	public static FurnitureEditMoveManager Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureEditMoveManager();
			return instance;
		}
	}
	
	private FurnitureEditMoveManager (){
		furnitureMoveUIPanel = GameObject.Find("FurnitureMoveUIPanel");
		popUp = GameObject.Find("PopUp");
		worldOrientation = GameObject.Find("World").transform.forward.normalized;
		angle = 10;
	}
	
	private GameObject furnitureMoveUIPanel;
	private GameObject selectedFurniture;
	private GameObject basedFurniture;
	private GameObject popUp;
	private Vector3 worldOrientation;
	private float angle;
	
	public void Initialize(GameObject basedFurniture, GameObject selectedFurniture){
		this.basedFurniture = basedFurniture;
		this.selectedFurniture = selectedFurniture;
		furnitureMoveUIPanel.renderer.enabled = true;
		SetCollider(furnitureMoveUIPanel,true);
		
	}
	
	private void SetCollider(GameObject gameObject, bool value){
		foreach (Transform child in gameObject.transform){
			if(child.tag.Equals("Touchable"))
				child.collider.enabled = value;
			if(child.childCount != 0)
				SetCollider( child.gameObject, value);			
		}
	}
	
	public void Close(){
		furnitureMoveUIPanel.renderer.enabled = false;
		SetCollider(furnitureMoveUIPanel,false);
	}
	
	public void DestroyBasedFurniture(){
		GameObject.Destroy(basedFurniture);
	}
	
	public void MoveUp(){
		selectedFurniture.transform.position = selectedFurniture.transform.position + new Vector3(worldOrientation.x,0,worldOrientation.z);
	}
	
	public void MoveDown(){
		selectedFurniture.transform.position = selectedFurniture.transform.position + new Vector3(-worldOrientation.x,0,-worldOrientation.z);
	}
	
	public void MoveLeft(){
		selectedFurniture.transform.position = selectedFurniture.transform.position + new Vector3(-worldOrientation.x,0,worldOrientation.z);
	}
	
	public void MoveRight(){
		selectedFurniture.transform.position = selectedFurniture.transform.position + new Vector3(worldOrientation.x,0,-worldOrientation.z);
	}
	
	public void RotateLeft(){
		//selectedFurniture.transform.Rotate(Vector3.up,(Mathf.PI/180.0)*angle);		
		//selectedFurniture.transform.Rotate(Vector3.up,UnityEngine.Space.);		
		selectedFurniture.transform.Rotate(0.0f, (float) (Mathf.PI/180.0)*angle,0.0f);
	}
	
	public void RotateRight(){
		selectedFurniture.transform.Rotate(0.0f, - (float) (Mathf.PI/180.0)*angle,0.0f);
	}
	
	public void PopUpUnplacableWarning(){
		//popUp.doSmth()
	}
	
	public bool isPlaceable(){
		//return activate method of selectedFurniture to get the current status of the furniture		
		return true;
	}

	
}
