﻿using UnityEngine;
using System.Collections;

public class FurnitureEditMoveState : State {

	private FurnitureEditMoveState (){		
		stateName = "FurnitureEditMoveState";		
	}
	
	private static FurnitureEditMoveState instance;
	
	public static FurnitureEditMoveState Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureEditMoveState();
			return instance;
		}
	}
	
	private FurnitureEditMoveManager furnitureEditMoveManager;
	
	public void Initialize(GameObject basedFurniture,GameObject selectedFurniture){
		furnitureEditMoveManager.Initialize(basedFurniture,selectedFurniture);
	}
	
	public override State TouchHandler(GameObject touchedObject){
		Debug.Log("FurnitureEditMoveState TouchHandler");
		if(touchedObject.name.Equals("MoveUp")){
			furnitureEditMoveManager.MoveUp();
		}
		else if(touchedObject.name.Equals("MoveDown")){
			furnitureEditMoveManager.MoveDown();
		}
		else if(touchedObject.name.Equals("MoveLeft")){
			furnitureEditMoveManager.MoveLeft();
		}
		else if(touchedObject.name.Equals("Right")){
			furnitureEditMoveManager.MoveRight();
		}
		else if(touchedObject.name.Equals("RotateRight")){
			furnitureEditMoveManager.RotateRight();
		}
		else if(touchedObject.name.Equals("RotateLeft")){
			furnitureEditMoveManager.RotateLeft();
		}
		else if(touchedObject.name.Equals("OK")){
			if(furnitureEditMoveManager.isPlaceable()){
				furnitureEditMoveManager.Close();
				furnitureEditMoveManager.DestroyBasedFurniture();
				return HouseViewState.Instance;			
			}
			else{
				furnitureEditMoveManager.PopUpUnplacableWarning();
				return this;
			}

		}
		else if(touchedObject.name.Equals("Back")){
			furnitureEditMoveManager.Close();
			FurnitureEditState nextState = FurnitureEditState.Instance;
			nextState.Initialize();
			return nextState;
		}
		return this;
	}
}
