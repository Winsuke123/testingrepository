﻿using UnityEngine;
using System.Collections;

public class FurnitureInfoState : State {

	private FurnitureInfoState (){
		stateName = "FurnitureInfoState";
		furnitureInfoManager = FurnitureInfoManager.Instance;
	}
	
	private static FurnitureInfoState instance;
	
	public static FurnitureInfoState Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureInfoState();
			return instance;
		}
	}
	
	private FurnitureInfoManager furnitureInfoManager;
	
	public void Initialize(){
		furnitureInfoManager.Initialize();
	}
	
	public void Initialize(GameObject basedFurniture){
		furnitureInfoManager.Initialize(basedFurniture);
	}
	
	public override State TouchHandler(GameObject touchedObject){
		Debug.Log("FurnitureInfoState TouchHandler");
		
		if(touchedObject.name.Equals("Edit")){			
			furnitureInfoManager.Close();	
			FurnitureEditState nextState = FurnitureEditState.Instance;
			nextState.Initialize(touchedObject);			
			return nextState;			
		}	
		else if(touchedObject.name.Equals("Back")){
			furnitureInfoManager.Close();
			AimingState nextState = AimingState.Instance;
			nextState.Initialize();
			return nextState;
		}
		else if(touchedObject.name.Equals("Exit")){
			furnitureInfoManager.Close();
			HouseViewState nextState = HouseViewState.Instance;
			return nextState;
		}
		
		return this;
		
	}
}
