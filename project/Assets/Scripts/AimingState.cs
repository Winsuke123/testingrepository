﻿using UnityEngine;
using System.Collections;

public class AimingState : State {
	
	
	private static AimingState instance;
	
	public static AimingState Instance
	{
		get
		{
			if (instance == null)
				instance = new AimingState();
			return instance;
		}
	}
	
	private AimingManager aimingManager;
	
	private AimingState (){
		stateName = "AimingState";
		aimingManager = AimingManager.Instance;
	}
	
	public void Initialize(){
		aimingManager.Initialize();
	}
			
	public override State AimHandler(GameObject targetedObject, string status){
		Debug.Log("AimingState AimHandler");
		
		if(status.Equals("Object found")){
			aimingManager.Close();
			if(targetedObject.tag.Equals("Furniture")){
				
			}
			else{
			
			}
		}
		else if(status.Equals("Lost marker")){
			aimingManager.Close();				
			return HouseViewState.Instance;
		}
		else if(status.Equals("Unknown")){
			aimingManager.PopUpWarning("Unknown object");
			return this;
		}
		else if(status.Equals("Out of range")){
			aimingManager.PopUpWarning("Object is out of range");
			return this;
		}		
		return this;
	}
	
}
