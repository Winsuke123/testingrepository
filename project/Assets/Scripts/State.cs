﻿using UnityEngine;
using System.Collections;

public abstract class State  {

	protected string stateName;
	
	public string StateName
	{
		get{ return stateName; }
	}
	
	public virtual State TouchHandler(GameObject touchedObject){	
		return this;
	}	
	
	public virtual State AimHandler(GameObject targetedObject, string status){
		return this;
	}
	
	public virtual State MagnaticTriggerHandler(){
		return this;
	}	
}
