﻿using UnityEngine;
using System.Collections;

public class FurnitureEditState : State {

	private FurnitureEditState (){
		stateName = "FurnitureEditState";
		furnitureEditManager = FurnitureEditManager.Instance;
		furnitureRemoveManager = FurnitureRemoveManager.Instance;
	}
	
	private static FurnitureEditState instance;
	
	public static FurnitureEditState Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureEditState();
			return instance;
		}
	}
	
	private FurnitureEditManager furnitureEditManager;
	private FurnitureRemoveManager furnitureRemoveManager;
	
	public void Initialize(){
		furnitureEditManager.Initialize();
	}
	
	public void Initialize(GameObject basedFurniture){
		furnitureEditManager.Initialize(basedFurniture);
	}
	
	private string FindParentUIName(GameObject gameObject){
		if(gameObject.tag.Equals("UIPanel"))
			return gameObject.name;
		return FindParentUIName(gameObject.transform.parent.gameObject);
	}
	
	public override State TouchHandler(GameObject touchedObject){
		Debug.Log("FurnitureEditState TouchHandler");
		
		if(touchedObject.name.Equals("Cancel")){
			if(FindParentUIName(touchedObject).Equals("FurnitureRemoveUIPanel")){
				furnitureRemoveManager.Close();
				furnitureEditManager.SetDefocus(false);
				return this;
			}
			else if(FindParentUIName(touchedObject).Equals("FurnitureEditUIPanel")){
				furnitureEditManager.Close();
				FurnitureInfoState nextState = FurnitureInfoState.Instance;
				nextState.Initialize();
				return nextState;
			}						
		}	
		else if(touchedObject.name.Equals("Move")){
			furnitureEditManager.Close();	
			FurnitureEditMoveState nextState = FurnitureEditMoveState.Instance;
			nextState.Initialize(furnitureEditManager.BasedFurniture,furnitureEditManager.CurrentFurniture);
			return nextState;
		}
		else if(touchedObject.name.Equals("Remove")){	
			furnitureEditManager.SetDefocus(true);
			furnitureRemoveManager.Initialize(furnitureEditManager.BasedFurniture,furnitureEditManager.CurrentFurniture);			
			return this;
		}
		else if(touchedObject.name.Equals("OK")){
			furnitureEditManager.Close();
			furnitureEditManager.SetDefocus(false);
			furnitureRemoveManager.Close();
			return HouseViewState.Instance;			
		}
		
		
		return this;
	}

}
