﻿using UnityEngine;
using System.Collections;

public class AutoWalkManager {

	private AutoWalkManager (){}
	
	private static AutoWalkManager instance;
	
	public static AutoWalkManager Instance
	{
		get
		{
			if (instance == null)
				instance = new AutoWalkManager();
			return instance;
		}
	}
	
	public void Initialize(){}
	
	public void Close(){}
}
