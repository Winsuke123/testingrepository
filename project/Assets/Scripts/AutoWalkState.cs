﻿using UnityEngine;
using System.Collections;

public class AutoWalkState : State {

	private AutoWalkState (){		
		stateName = "AutoWalkState";
		autoWalkManager = AutoWalkManager.Instance;
	}
	
	private static AutoWalkState instance;
	
	public static AutoWalkState Instance
	{
		get
		{
			if (instance == null)
				instance = new AutoWalkState();
			return instance;
		}
	}
	
	private AutoWalkManager autoWalkManager;
	
	public void initialize(){
		autoWalkManager.Initialize();
	}
	
	public override State MagnaticTriggerHandler(){
		Debug.Log("AutoWalkState MagnaticTriggerHandler");
		
		autoWalkManager.Close();
		
		return HouseViewState.Instance;
	}
	
}
