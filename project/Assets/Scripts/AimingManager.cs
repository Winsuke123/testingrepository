﻿using UnityEngine;
using System.Collections;

public class AimingManager {

	private GameObject popUp;
	private GameObject aimingRay;
	
	private AimingManager (){
		popUp = GameObject.Find("PopUp");
		aimingRay = GameObject.Find("AimingRay");
	}
	
	private static AimingManager instance;
	
	public static AimingManager Instance
	{
		get
		{
			if (instance == null)
				instance = new AimingManager();
			return instance;
		}
	}
	
	public void Initialize(){		
		aimingRay.renderer.enabled = true;
	}
	
	public void Close(){
		aimingRay.renderer.enabled = false;
	}
	
	public void PopUpWarning(string warningText){
		//activate method of popUp GameObject with warning text as parameter.
	}
}
