﻿using UnityEngine;
using System.Collections;

public class FurnitureRemoveManager {

	private static FurnitureRemoveManager instance;
	
	public static FurnitureRemoveManager Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureRemoveManager();
			return instance;
		}
	}
	
	private FurnitureRemoveManager (){
		furnitureRemoveUIPanel = GameObject.Find("FurnitureRemoveUIPanel");						
	}
	private GameObject furnitureRemoveUIPanel;
	private GameObject basedFurniture;
	private GameObject currentFurniture;
	
	public void Initialize(GameObject basedFurniture, GameObject currentFurniture){
		this.basedFurniture = basedFurniture;
		this.currentFurniture = currentFurniture;
		furnitureRemoveUIPanel.renderer.enabled = true;
		SetCollider(furnitureRemoveUIPanel,true);
		
	}
	
	public void Close(){
		furnitureRemoveUIPanel.renderer.enabled = false;
		SetCollider(furnitureRemoveUIPanel,false);	
	}
	
	private void SetCollider(GameObject gameObject, bool value){
		foreach (Transform child in gameObject.transform){
			if(child.tag.Equals("Touchable"))
				child.collider.enabled = value;
			if(child.childCount != 0)
				SetCollider( child.gameObject, value);			
		}
	}
	
	public void Remove(){
		GameObject.Destroy(basedFurniture);
		GameObject.Destroy(currentFurniture);
	}
}
