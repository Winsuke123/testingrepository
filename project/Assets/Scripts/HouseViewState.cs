﻿using UnityEngine;
using System.Collections;

public class HouseViewState : State {

	private HouseViewState (){
		stateName = "HouseViewState";
	}
	
	private static HouseViewState instance;
	
	public static HouseViewState Instance
	{
		get
		{
			if (instance == null)
				instance = new HouseViewState();
			return instance;
		}
	}
	
	public void initialize(){
		
	}
	
	public override State TouchHandler(GameObject touchedObject){
		Debug.Log("HouseViewState TouchHandler");
		
		MainMenuState nextState = MainMenuState.Instance;
		nextState.initialize();
		return nextState;
	}
	
	public override State MagnaticTriggerHandler(){
		Debug.Log("HouseViewState MagnaticTriggerHandler");
		
		AutoWalkState nextState = AutoWalkState.Instance;
		nextState.initialize();
		return nextState;
	}
}
