﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FurnitureEditManager  {

	
	private static FurnitureEditManager instance;
	
	public static FurnitureEditManager Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureEditManager();
			return instance;
		}
	}
	
	private FurnitureEditManager (){
		furnitureEditUIPanel = GameObject.Find("FurnitureEditUIPanel");		
		furnitureInfoBox = GameObject.Find("FurnitureInfoBox");
		popUp = GameObject.Find("PopUp");
		furnitureDAO = FurnitureDAO.Instance;
		currentPage = 0;
	}
	
	private GameObject furnitureEditUIPanel;
	private GameObject basedFurniture;
	private GameObject currentFurniture;
	private GameObject furnitureInfoBox;
	private GameObject popUp;
	//private List<FurnitureStruct> furnitureStructs;
	private FurnitureDAO furnitureDAO;
	private List<GameObject> furnitures;
	private List<FurnitureDetail> furnitureDetails;
	private int currentPage;
	
	public GameObject CurrentFurniture
	{
		get
		{
			return this.currentFurniture;
		}
	}
	
	public GameObject BasedFurniture
	{
		get
		{
			return this.basedFurniture;
		}
	}
	
	public void Initialize(){
		furnitureEditUIPanel.renderer.enabled = true;
		SetCollider(furnitureEditUIPanel,true);
		currentFurniture.transform.position = this.basedFurniture.transform.position;
		currentFurniture.transform.rotation = this.basedFurniture.transform.rotation;		
		
	}
	
	public void Initialize(GameObject basedFurniture){
		furnitureEditUIPanel.renderer.enabled = true;
		SetCollider(furnitureEditUIPanel,true);				
		this.basedFurniture = basedFurniture;		
		
		reloadFurniture();		
		setFurnitureToEditUIPanel();

	}
	
	public void Close(){
		furnitureEditUIPanel.renderer.enabled = false;
		SetCollider(furnitureEditUIPanel,false);				
	}
	
	private void SetCollider(GameObject gameObject, bool value){
		foreach (Transform child in gameObject.transform){
			if(child.tag.Equals("Touchable"))
				child.collider.enabled = value;
			if(child.childCount != 0)
				SetCollider( child.gameObject, value);			
		}
	}
	
	public void SetCurrentFurniture(GameObject currentFurniture){
		GameObject.Destroy(this.currentFurniture);
		this.currentFurniture = currentFurniture;
		GameObject.Instantiate(this.currentFurniture,this.basedFurniture.transform.position,this.basedFurniture.transform.rotation);
		
		
		SetFurnituredInfoBox(findDetail(currentFurniture.name));
	}
	
	private FurnitureDetail findDetail(string furnitureId){
		foreach( FurnitureDetail furnitureDetail in furnitureDetails){
			if(furnitureDetail.furnitureId.Equals(furnitureId))
				return furnitureDetail;
		}
		return new FurnitureDetail();
	}
	
	private void SetFurnituredInfoBox(FurnitureDetail furnitureDetail){
		/* activate method in furnitureInfoBox to set the current furniture detail				
		this.furnitureInfoBox._____(furnitureDetail.name,
										furnitureDetail.description,
										furnitureDetail.manufacture,
										furnitureDetail.dimension,
										furnitureDetail.price);
		*/
	}
	
	public void SwitchFurnitureInfoBoxVisibility(){
		this.furnitureInfoBox.renderer.enabled = !(this.furnitureInfoBox.renderer.enabled); 
	}
	
	public void NextPage(){
		this.currentPage++;
		reloadFurniture();
		setFurnitureToEditUIPanel();
	}
	
	public void PreviousPage(){
		this.currentPage--;
		reloadFurniture();
		setFurnitureToEditUIPanel();
	}
	
	private void setFurnitureToEditUIPanel(){
		// activate method of furnitureEditUIPanel to set all of the displayed furniture
	}
	
	private void reloadFurniture(){
		furnitures.Clear(); // need to destroy GameObject?
		furnitureDetails.Clear();
		foreach (FurnitureStruct furnitureStructure in this.furnitureDAO.find(currentPage)){
			furnitures.Add(furnitureStructure.furniture);
			furnitureDetails.Add(furnitureStructure.detail);
		}
	}
	
	public void SetDefocus(bool value){
		SetCollider(furnitureEditUIPanel,!value);
		//set transparent of furnitureEditUIPanel depending on the argument's value
	}
}
