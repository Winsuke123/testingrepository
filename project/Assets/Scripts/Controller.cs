﻿using UnityEngine;
using System.Collections;

public class Controller{

	private static Controller instance;
	
	private Controller(){
		currentState = HouseViewState.Instance;
	}
	
	public static Controller Instance
	{
		get
		{
			if (instance == null)
				instance = new Controller();
			return instance;
		}
	}
	
	private State currentState;
	
	public void TouchAction(GameObject touchedObject)
	{
		currentState = currentState.TouchHandler(touchedObject);
		Debug.Log("Controller " + GetCurrentState());
	}
	
	public void AimAction(GameObject targetedObject, string status)
	{
		currentState = currentState.AimHandler(targetedObject,status);
		Debug.Log("Controller " + GetCurrentState());
	}
	
	public void MagnaticTriigerAction()
	{
		currentState = currentState.MagnaticTriggerHandler();
		Debug.Log("Controller " + GetCurrentState());
	}
	
	public string GetCurrentState(){
		return currentState.StateName;
	}
}
