﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct FurnitureStruct
{
	public GameObject furniture;
	public FurnitureDetail detail;
};

public struct FurnitureDetail
{
	public string furnitureId;
	public string name;
	public string description;
	public string manufacture;
	public string dimension;
	public float price;		
};

public class FurnitureDAO {

	private FurnitureDAO (){
		//set db connection
	}
	
	private static FurnitureDAO instance;
	
	public static FurnitureDAO Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureDAO();
			return instance;
		}
	}
	
	public List<FurnitureStruct> find(){
		return null;
	}
	
	public List<FurnitureStruct> find(int page){
		return null;
	}	
	
	// use in view info screen?
	public FurnitureDetail find(string name){
		return new FurnitureDetail();
	}
}
