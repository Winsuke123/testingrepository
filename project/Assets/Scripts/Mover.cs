﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {

	
	private float speed;
	
	// Use this for initialization
	void Start () {
		this.speed = 2.0f;	
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("FurnitureContainer object, method : move");
		float horizontalAxis = Input.GetAxis ("Horizontal");
		float verticalAxis = Input.GetAxis ("Vertical");
		
		this.gameObject.transform.position += new Vector3 (horizontalAxis,0.0f,verticalAxis)*Time.deltaTime*speed;
	}
}
