﻿using UnityEngine;
using System.Collections;

public class runner : MonoBehaviour {

	private Controller controller;
	private float t;
	
	void Start () {
		controller = Controller.Instance;
		t = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if( t > 1){
			if(Input.GetKey("a")){
				controller.AimAction(null,"");
				t=0;
			}
			else if(Input.GetKey("m")){
				controller.MagnaticTriigerAction();
				t=0;
			}
			else if(Input.GetKey("t")){
				controller.TouchAction(null);
				t=0;
			}
		}
		t+=Time.deltaTime;
	}		
}
