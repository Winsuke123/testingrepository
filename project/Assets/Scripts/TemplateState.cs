﻿using UnityEngine;
using System.Collections;

public class TemplateState : State {

	private TemplateState (){
		stateName = "TemplateState";
	}
	
	private static TemplateState instance;
	
	public static TemplateState Instance
	{
		get
		{
			if (instance == null)
				instance = new TemplateState();
			return instance;
		}
	}
	
	public void initialize(){
		
	}
	
	public override State TouchHandler(GameObject touchedObject){
		Debug.Log("TemplateState TouchHandler");
		return null;
	}
	
	public override State AimHandler(GameObject targetedObject, string status){
		Debug.Log("TemplateState AimHandler");
		return null;
	}
	
	public override State MagnaticTriggerHandler(){
		Debug.Log("TemplateState MagnaticTriggerHandler");
		return null;
	}
	
}
