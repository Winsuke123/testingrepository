﻿using UnityEngine;
using System.Collections;

public class FurnitureInfoManager {
	
	
	private static FurnitureInfoManager instance;
	
	public static FurnitureInfoManager Instance
	{
		get
		{
			if (instance == null)
				instance = new FurnitureInfoManager();
			return instance;
		}
	}
	
	private FurnitureInfoManager (){
		furnitureInfoUIPanel = GameObject.Find("FurnitureInfoUIPanel");		
		furnitureDAO = FurnitureDAO.Instance;
	}
	
	private GameObject furnitureInfoUIPanel;
	private GameObject selectedFurniture;
	private FurnitureDAO furnitureDAO;
	private FurnitureDetail furnitureDetail;
	
	public void Initialize(){
		furnitureInfoUIPanel.renderer.enabled = true;
		SetCollider(furnitureInfoUIPanel,true);
	}
	
	public void Initialize(GameObject basedFurniture){

		furnitureDetail = furnitureDAO.find(basedFurniture.name);
		/* for add the based furniture and its detail to the panel
		
		furnitureInfoUIPanel._____(basedFurniture) 
		furnitureInfoUIPanel._____(	furnitureDetail.name,
									furnitureDetail.description,
									furnitureDetail.manufacture,
									furnitureDetail.dimension,
									furnitureDetail.price);
		*/

		Initialize();
	}
	
	public void Close(){
		furnitureInfoUIPanel.renderer.enabled = false;
		SetCollider(furnitureInfoUIPanel,false);
	}
	
	private void SetCollider(GameObject gameObject, bool value){
		foreach (Transform child in gameObject.transform){
			if(child.tag.Equals("Touchable"))
				child.collider.enabled = value;
			if(child.childCount != 0)
				SetCollider( child.gameObject, value);			
		}
	}
	
}
