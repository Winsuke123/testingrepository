﻿using UnityEngine;
using System.Collections;

public class MainMenuState : State {

	private MainMenuState (){
		stateName = "MainMenuState";
		mainMenuManager = MainMenuManager.Instance;
	}
	
	private static MainMenuState instance;
	
	public static MainMenuState Instance
	{
		get
		{
			if (instance == null)
				instance = new MainMenuState();
			return instance;
		}
	}
	
	private MainMenuManager mainMenuManager;
	
	public void initialize(){
		mainMenuManager.Initialize();
	}
	
	public override State TouchHandler(GameObject touchedObject){
		Debug.Log("MainMenuState TouchHandler");	
		if(touchedObject.name.Equals("Select")){
			
			mainMenuManager.Close();
			
			AimingState nextState = AimingState.Instance;
			nextState.Initialize();			
			return nextState;
			
		}			
		
		else if(touchedObject.name.Equals("Add Furniture")){
		
		}
		
		else if(touchedObject.name.Equals("Back")){
			mainMenuManager.Close();
			HouseViewState nextState = HouseViewState.Instance;
			return nextState;
		}
		
		return this;
		
	}
	
}
